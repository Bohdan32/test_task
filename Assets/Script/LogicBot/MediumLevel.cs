﻿using System.Collections.Generic;
using UnityEngine;

public class MediumLevel
{
    private List<Vector2> emptyCell = new List<Vector2>();

    //Bot medium level, before running, checks are there any combinations where is not enough one element to win. 
    //And this can be as an element of the player as the element bot.
    //If there is such a combination - makes a move.
    //Else - move to a free cell.

    public void BotMediumLevel(eTarget target , ref CellInBoard[,] _cellInBoard)
    {
        //See comments in script "EasyLevel"
        emptyCell.Clear();

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (_cellInBoard[i, j].TargetCell == eTarget.empty)
                {
                    emptyCell.Add(new Vector2(i, j));
                }
            }
        }
        if (emptyCell.Count == 0)
        {
            ProfilePlayer.EditLastRound(0);

            UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(0);

            UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);
        }
        else
        {

            if (AnalizRunPlayerMediumLevel(target , ref _cellInBoard) == true)
            {

            }
            else
            {
                int r = (int)UnityEngine.Random.Range(0, emptyCell.Count - 1);

                if (_cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].TargetCell == eTarget.empty)
                {
                    _cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].TargetCell = target;

                    _cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].SelectCell();
                }
            }
   
        }
    }

    //Field analysis medium level. We compare two elements,if they are the same and third - empty - move in third.
    private bool AnalizRunPlayerMediumLevel(eTarget target , ref CellInBoard[,] _cellInBoard)
    {
        bool result = false;

        for (int i = 0; i < 3; i++)
        {
            if (_cellInBoard[i, 0].TargetCell == _cellInBoard[i, 1].TargetCell && _cellInBoard[i, 0].TargetCell != eTarget.empty) //Analiz all vertical line 1 and 2 cells
            {
                if (_cellInBoard[i, 2].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[i, 2].TargetCell = target;

                    _cellInBoard[i, 2].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[i, 1].TargetCell == _cellInBoard[i, 2].TargetCell && _cellInBoard[i, 2].TargetCell != eTarget.empty) //Analiz all vertical line 2 and 3 cells
            {
                if (_cellInBoard[i, 0].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[i, 0].TargetCell = target;

                    _cellInBoard[i, 0].SelectCell();
                    break;
                }
            }

            if (_cellInBoard[0, i].TargetCell == _cellInBoard[1, i].TargetCell && _cellInBoard[0, i].TargetCell != eTarget.empty)//Analiz all horizontal line 1 and 2 cells
            {
                if (_cellInBoard[2, i].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[2, i].TargetCell = target;

                    _cellInBoard[2, i].SelectCell();

                    break;
                }
            }


            if (_cellInBoard[1, i].TargetCell == _cellInBoard[2, i].TargetCell && _cellInBoard[1, i].TargetCell != eTarget.empty)//Analiz all horizontal line 2 and 3 cells
            {
                if (_cellInBoard[0, i].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[0, i].TargetCell = target;

                    _cellInBoard[0, i].SelectCell();

                    break;
                }
            }
            //Diagonals
            if (_cellInBoard[0, 0].TargetCell == _cellInBoard[1, 1].TargetCell && _cellInBoard[0, 0].TargetCell != eTarget.empty)//Down from right to left 1 and 2 elements
            {
                if (_cellInBoard[2, 2].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[2, 2].TargetCell = target;

                    _cellInBoard[2, 2].SelectCell();

                    break;
                }
            }


            if (_cellInBoard[1, 1].TargetCell == _cellInBoard[2, 2].TargetCell && _cellInBoard[1, 1].TargetCell != eTarget.empty)//Down from right to left 2 and 3 elements
            {
                if (_cellInBoard[0, 0].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[0, 0].TargetCell = target;

                    _cellInBoard[0, 0].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[0, 2].TargetCell == _cellInBoard[1, 1].TargetCell && _cellInBoard[0, 2].TargetCell != eTarget.empty)//Up from left to right 1 and 2 elements 
            {
                if (_cellInBoard[2, 0].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[2, 0].TargetCell = target;

                    _cellInBoard[2, 0].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[1, 1].TargetCell == _cellInBoard[2, 0].TargetCell && _cellInBoard[1, 1].TargetCell != eTarget.empty)//Up from left to right 2 and 3 elements 
            {
                if (_cellInBoard[0, 2].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[0, 2].TargetCell = target;

                    _cellInBoard[0, 2].SelectCell();

                    break;
                }
            }
        }
        return result;
    }

}
