﻿using System.Collections.Generic;
using UnityEngine;

public class HardLevel : MonoBehaviour
{
    private List<Vector2> emptyCell = new List<Vector2>();

    //The hard level bot first do analiz of him cell. If he now will make a move and can win, he does it. 
    //If previous analiz false, he do analiz of user moves. Bot closes the last cell of the user.
    //Else - move to a free cell.

    public void BotHardLevel(eTarget target, ref CellInBoard[,] _cellInBoard)
    {
        // //See comments in script "EasyLevel"
        emptyCell.Clear();

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (_cellInBoard[i, j].TargetCell == eTarget.empty)
                {
                    emptyCell.Add(new Vector2(i, j));
                }
            }
        }

        if (emptyCell.Count == 0)
        {
            ProfilePlayer.EditLastRound(0);

            UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(0);

            UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);
        }
        else
        {
            if (AnalizRunBotHardLevel(ref _cellInBoard , target) == true)
            { }
            else
            {
                if (AnalizRunPlayerHardLevel(ref _cellInBoard, target) == true)
                { }
                else
                {
                    int r = (int)UnityEngine.Random.Range(0, emptyCell.Count - 1);

                    if (_cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].TargetCell == eTarget.empty)
                    {
                        _cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].TargetCell = target;

                        _cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].SelectCell();
                    }
                }
            }
        }
    }

    //Analiz of bot cell
    private bool AnalizRunBotHardLevel( ref CellInBoard[,] _cellInBoard, eTarget BotTarget)
    {
        bool result = false;

        for (int i = 0; i < 3; i++)
        {
            if (_cellInBoard[i, 0].TargetCell == _cellInBoard[i, 1].TargetCell && _cellInBoard[i, 0].TargetCell == BotTarget) 
            {

                if (_cellInBoard[i, 2].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[i, 2].TargetCell = BotTarget;

                    _cellInBoard[i, 2].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[i, 1].TargetCell == _cellInBoard[i, 2].TargetCell && _cellInBoard[i, 2].TargetCell == BotTarget)
            {
                if (_cellInBoard[i, 0].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[i, 0].TargetCell = BotTarget;

                    _cellInBoard[i, 0].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[0, i].TargetCell == _cellInBoard[1, i].TargetCell && _cellInBoard[0, i].TargetCell == BotTarget)
            {
                if (_cellInBoard[2, i].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[2, i].TargetCell = BotTarget;

                    _cellInBoard[2, i].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[1, i].TargetCell == _cellInBoard[2, i].TargetCell && _cellInBoard[1, i].TargetCell == BotTarget)
            {
                if (_cellInBoard[0, i].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[0, i].TargetCell = BotTarget;

                    _cellInBoard[0, i].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[0, 0].TargetCell == _cellInBoard[1, 1].TargetCell && _cellInBoard[0, 0].TargetCell == BotTarget)
            {
                if (_cellInBoard[2, 2].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[2, 2].TargetCell = BotTarget;

                    _cellInBoard[2, 2].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[1, 1].TargetCell == _cellInBoard[2, 2].TargetCell && _cellInBoard[1, 1].TargetCell == BotTarget)
            {
                if (_cellInBoard[0, 0].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[0, 0].TargetCell = BotTarget;

                    _cellInBoard[0, 0].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[0, 2].TargetCell == _cellInBoard[1, 1].TargetCell && _cellInBoard[0, 2].TargetCell == BotTarget)
            {
                if (_cellInBoard[2, 0].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[2, 0].TargetCell = BotTarget;

                    _cellInBoard[2, 0].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[1, 1].TargetCell == _cellInBoard[2, 0].TargetCell && _cellInBoard[1, 1].TargetCell == BotTarget) 
            {
                if (_cellInBoard[0, 2].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[0, 2].TargetCell = BotTarget;

                    _cellInBoard[0, 2].SelectCell();

                    break;
                }
            }
        }
        return result;
    }

    //Analiz of player cell
    private bool AnalizRunPlayerHardLevel(ref CellInBoard[,] _cellInBoard, eTarget BotTarget)
    {
        bool result = false;

        for (int i = 0; i < 3; i++)
        {
            if (_cellInBoard[i, 0].TargetCell == _cellInBoard[i, 1].TargetCell && _cellInBoard[i, 0].TargetCell != BotTarget && _cellInBoard[i, 0].TargetCell != eTarget.empty) 
            {
                if (_cellInBoard[i, 2].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[i, 2].TargetCell = BotTarget;

                    _cellInBoard[i, 2].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[i, 1] == _cellInBoard[i, 2] && _cellInBoard[i, 2].TargetCell != BotTarget && _cellInBoard[i, 2].TargetCell != eTarget.empty) 
            {
                if (_cellInBoard[i, 0].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[i, 0].TargetCell = BotTarget;

                    _cellInBoard[i, 0].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[0, i].TargetCell == _cellInBoard[1, i].TargetCell && _cellInBoard[0, i].TargetCell != BotTarget && _cellInBoard[0, i].TargetCell != eTarget.empty)
            {
                if (_cellInBoard[2, i].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[2, i].TargetCell = BotTarget;

                    _cellInBoard[2, i].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[1, i].TargetCell == _cellInBoard[2, i].TargetCell && _cellInBoard[1, i].TargetCell != BotTarget && _cellInBoard[1, i].TargetCell != eTarget.empty)
            {
                if (_cellInBoard[0, i].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[0, i].TargetCell = BotTarget;

                    _cellInBoard[0, i].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[0, 0].TargetCell == _cellInBoard[1, 1].TargetCell && _cellInBoard[0, 0].TargetCell != BotTarget && _cellInBoard[0, 0].TargetCell != eTarget.empty)  
            {
                if (_cellInBoard[2, 2].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[2, 2].TargetCell = BotTarget;

                    _cellInBoard[2, 2].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[1, 1].TargetCell == _cellInBoard[2, 2].TargetCell && _cellInBoard[1, 1].TargetCell != BotTarget && _cellInBoard[1, 1].TargetCell != eTarget.empty) 
            {
                if (_cellInBoard[0, 0].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[0, 0].TargetCell = BotTarget;

                    _cellInBoard[0, 0].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[0, 2].TargetCell == _cellInBoard[1, 1].TargetCell && _cellInBoard[0, 2].TargetCell != BotTarget && _cellInBoard[0, 2].TargetCell != eTarget.empty)  
            {
                if (_cellInBoard[2, 0].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[2, 0].TargetCell = BotTarget;

                    _cellInBoard[2, 0].SelectCell();

                    break;
                }
            }

            if (_cellInBoard[1, 1].TargetCell == _cellInBoard[2, 0].TargetCell && _cellInBoard[1, 1].TargetCell != BotTarget && _cellInBoard[1, 1].TargetCell != eTarget.empty)
            {
                if (_cellInBoard[0, 2].TargetCell == eTarget.empty)
                {
                    result = true;

                    _cellInBoard[0, 2].TargetCell = BotTarget;

                    _cellInBoard[0, 2].SelectCell();

                    break;
                }
            }
        }
        return result;
    }
}
