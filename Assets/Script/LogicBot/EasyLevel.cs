﻿using System.Collections.Generic;
using UnityEngine;

public class EasyLevel : MonoBehaviour
{
    private List<Vector2> emptyCell = new List<Vector2>();

    //The first level bot is doing random moves to free cells
    public void BotEasyLevel(eTarget target , ref CellInBoard [,] _cellInBoard )
    {
        // First we must check  empty cell. Maybe they are not - draw. If they are, we know where bot can move

        emptyCell.Clear();

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (_cellInBoard[i, j].TargetCell == eTarget.empty)
                {
                    emptyCell.Add(new Vector2(i, j));// Add here all available position, that bot knows where here can move
                }
            }
        }
        if (emptyCell.Count == 0)// Check the draw
        {
            ProfilePlayer.EditLastRound(0);

            UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(0);

            UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);
        }
        else
        {
            int r = (int)UnityEngine.Random.Range(0, emptyCell.Count - 1);

            if (_cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].TargetCell == eTarget.empty)
            {
                _cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].TargetCell = target;

                _cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].SelectCell();
            }
        }
        
    }
}
